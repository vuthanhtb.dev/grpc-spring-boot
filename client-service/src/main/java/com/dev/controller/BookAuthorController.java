package com.dev.controller;

import com.dev.service.BookAuthorService;
import com.google.protobuf.Descriptors;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/author")
@RequiredArgsConstructor
public class BookAuthorController {
    private final BookAuthorService service;

    @GetMapping("/{id}")
    public Map<Descriptors.FieldDescriptor, Object> getAuthor(@PathVariable int id) {
        return this.service.getAuthor(id);
    }

    @GetMapping("/{id}/books")
    public List<Map<Descriptors.FieldDescriptor, Object>> getBooksByAuthor(@PathVariable int id) throws InterruptedException {
        return this.service.getBooksByAuthor(id);
    }

    @GetMapping("/expensive-book")
    public Map<String, Map<Descriptors.FieldDescriptor, Object>> getExpensiveBook() throws InterruptedException {
        return this.service.getExpensiveBook();
    }

    @GetMapping("/books/{gender}")
    public List<Map<Descriptors.FieldDescriptor, Object>> getBookByAuthorGender(@PathVariable String gender) throws InterruptedException {
        return this.service.getBookByAuthorGender(gender);
    }
}
