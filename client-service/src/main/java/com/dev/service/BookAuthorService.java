package com.dev.service;

import com.dev.Author;
import com.dev.Book;
import com.dev.BookAuthorServiceGrpc;
import com.dev.TempDB;
import com.google.protobuf.Descriptors;
import io.grpc.ManagedChannel;
import io.grpc.stub.StreamObserver;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Service
public class BookAuthorService {

    BookAuthorServiceGrpc.BookAuthorServiceBlockingStub syncClient;
    BookAuthorServiceGrpc.BookAuthorServiceStub asyncClient;

    public BookAuthorService(ManagedChannel managedChannel) {
        this.syncClient = BookAuthorServiceGrpc.newBlockingStub(managedChannel);
        this.asyncClient = BookAuthorServiceGrpc.newStub(managedChannel);
    }

    public Map<Descriptors.FieldDescriptor, Object> getAuthor(int authorId) {
        Author authorRequest = Author.newBuilder().setAuthorId(authorId).build();
        Author authorResponse = this.syncClient.getAuthor(authorRequest);
        return authorResponse.getAllFields();
    }

    public List<Map<Descriptors.FieldDescriptor, Object>> getBooksByAuthor(int authorId) throws InterruptedException {
        Author authorRequest = Author.newBuilder().setAuthorId(authorId).build();

        final List<Map<Descriptors.FieldDescriptor, Object>> books = new ArrayList<>();
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        this.asyncClient.getBooksByAuthor(authorRequest, new StreamObserver<Book>() {
            @Override
            public void onNext(Book book) {
                books.add(book.getAllFields());
            }

            @Override
            public void onError(Throwable throwable) {
                countDownLatch.countDown();
            }

            @Override
            public void onCompleted() {
                countDownLatch.countDown();
            }
        });

        boolean await = countDownLatch.await(1, TimeUnit.MINUTES);
        return await ? books : Collections.emptyList();
    }

    public Map<String, Map<Descriptors.FieldDescriptor, Object>> getExpensiveBook() throws InterruptedException {
        final Map<String, Map<Descriptors.FieldDescriptor, Object>> response = new HashMap<>();
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        StreamObserver<Book> responseObserver = this.asyncClient.getExpensiveBook(new StreamObserver<Book>() {
            @Override
            public void onNext(Book book) {
                response.put("ExpensiveBook", book.getAllFields());
            }

            @Override
            public void onError(Throwable throwable) {
                countDownLatch.countDown();
            }

            @Override
            public void onCompleted() {
                countDownLatch.countDown();
            }
        });

        TempDB.getBooksFromTempDb().forEach(responseObserver::onNext);
        responseObserver.onCompleted();

        boolean await = countDownLatch.await(1, TimeUnit.MINUTES);
        return await ? response : Collections.emptyMap();
    }

    public List<Map<Descriptors.FieldDescriptor, Object>> getBookByAuthorGender(String gender) throws InterruptedException {
        final List<Map<Descriptors.FieldDescriptor, Object>> response = new ArrayList<>();
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        StreamObserver<Book> responseObserver = this.asyncClient.getBookByAuthorGender(new StreamObserver<Book>() {
            @Override
            public void onNext(Book book) {
                response.add(book.getAllFields());
            }

            @Override
            public void onError(Throwable throwable) {
                countDownLatch.countDown();
            }

            @Override
            public void onCompleted() {
                countDownLatch.countDown();
            }
        });

        TempDB.getAuthorsFromTempDb()
                .stream()
                .filter(author -> author.getGender().equalsIgnoreCase(gender))
                .forEach(author -> responseObserver.onNext(Book.newBuilder().setAuthorId(author.getAuthorId()).build()));

        responseObserver.onCompleted();

        boolean await = countDownLatch.await(1, TimeUnit.MINUTES);
        return await ? response : Collections.emptyList();
    }
}
